### ============================================================================
### ============================================================================
### ============================================================================
### NSAS single fleet assessment
### ============================================================================
### ============================================================================
### ============================================================================

rm(list=ls()); graphics.off(); start.time <- proc.time()[3]
options(stringsAsFactors=FALSE)
log.msg     <-  function(string) {cat(string);}
log.msg("\nNSH Final Assessment (single fleet)\n=====================\n")

# local path
#path <- "D:/Repository/ICES_HAWG/wg_HAWG/NSAS/"
#path <- "C:/git/wg_HAWG/NSAS/"
path <- "J:/git/heras_index_kbwot/assessment/SA_error"
#path <- "C:/git/2020_her.27.3a47d_multifleet/"
#path <- "C:/git/2020_her.27.3a47d_assessment/"
#path <- "C:/git/2020_her.27.3a47d_forecast/"
#path <- "D:/git/wg_HAWG/NSAS/"
try(setwd(path),silent=TRUE)

### ======================================================================================================
### Define parameters and paths for use in the assessment code
### ======================================================================================================
dir.create("assessment",showWarnings = FALSE)

output.dir          <-  file.path(".","assessment/")              # result directory\
script.dir          <-  file.path(".","side_scripts/")            # result directory
n.retro.years       <-  6                                        # Number of years for which to run the retrospective
assessment_name     <- 'NSH_HAWG2020_sf_SA_error_2017_2019'
#assessment_name     <- 'NSH_HAWG2020_sf'


load(file.path(output.dir,'NSH_HAWG2020_sf_start.Rdata'))

load('J:/git/heras_index_kbwot/results/HERAS_sensitivity_SA_error.RData')

### ============================================================================
### imports
### ============================================================================
library(FLSAM); library(FLEDA)

rep_year <- ac(2017:2019)

source(file.path(script.dir,"setupAssessmentObjects_sf.r"))
source(file.path(script.dir,"setupControlObject_sf.r"))

### ============================================================================
### ============================================================================
### ============================================================================
### Assessment
### ============================================================================
### ============================================================================
### ============================================================================
NSH.sam               <- FLSAM(NSH,NSH.tun,NSH.ctrl,starting.values = stk0.NSH.sam)#,starting.values = NSH.sam
NSH@stock.n           <- NSH.sam@stock.n[,ac(range(NSH)["minyear"]:range(NSH)["maxyear"])]
NSH@harvest           <- NSH.sam@harvest[,ac(range(NSH)["minyear"]:range(NSH)["maxyear"])]


save(NSH,
     NSH.tun,
     NSH.ctrl,
     NSH.sam,
     file=file.path(output.dir,paste0(assessment_name,'.Rdata')))


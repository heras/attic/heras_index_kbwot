clear all
close all
clc

path = pwd;

stoxFile1 = xml2struct(fullfile(path,'Biotic_26D4201806_StoX - ABNsetEMPTY.xml'));
% stoxFile1 = xml2struct(fullfile(path,'Biotic_NLHERAS2018_StoX.xml'));
% stoxFile1 = xml2struct('D:\HERAS\data\StoX_projects\2019\HERAS_2019_HER_EU\input\biotic\Biotic_DK_26D4201908_StoX.xml');

nStations = length(stoxFile1.mission.fishstation);

specimenNo = struct();
for idxStation = 1:nStations
    nSpecies = length(stoxFile1.mission.fishstation{idxStation}.catchsample);
    
    for idxSpecies = 1:nSpecies
        count = 0;
        if length(stoxFile1.mission.fishstation{idxStation}.catchsample) > 1
            if isfield(stoxFile1.mission.fishstation{idxStation}.catchsample{idxSpecies},'individual')
                nIndividuals = length(stoxFile1.mission.fishstation{idxStation}.catchsample{idxSpecies}.individual);
                for idxInd = 1:nIndividuals
                    if length(stoxFile1.mission.fishstation{idxStation}.catchsample{idxSpecies}.individual) ~=1
                        specimenNo(idxStation).species(idxSpecies).specimenno(idxInd) = str2double(stoxFile1.mission.fishstation{idxStation}.catchsample{idxSpecies}.individual{idxInd}.Attributes.specimenno);
                    else
                        specimenNo(idxStation).species(idxSpecies).specimenno(idxInd) = str2double(stoxFile1.mission.fishstation{idxStation}.catchsample{idxSpecies}.individual(idxInd).Attributes.specimenno);
                    end
                end
                
               if (length(specimenNo(idxStation).species(idxSpecies).specimenno) - length(unique(specimenNo(idxStation).species(idxSpecies).specimenno))) ~= 0
                   count = count + 1;
                   specimenNo(idxStation).flag      = 1;
                   specimenNo(idxStation).flagIdx   = idxSpecies;
               end
            end
        end
    end
end



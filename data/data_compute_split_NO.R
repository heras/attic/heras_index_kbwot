######################################################################
######################################################################
######################################################################
# compute NSAS/WBSS split proportions for the Norwegian StoX project
######################################################################
######################################################################
######################################################################

rm(list=ls())

library(tidyverse)

path <- 'J:/git/heras_index_kbwot/data'

try(setwd(path),silent=TRUE)

#load(file.path(dataPath,"HERAS_NO_split_2016_2020_old.Rdata"))
load("./miscellaneous/HERAS data 2014_2020.Rdata")

HERAS <- HERAS %>% filter(lengthsamplecount>=30)
HERAS <- HERAS %>% filter(longitudestart>=2 & longitudestart<=6)

split_prop <- HERAS %>% filter(Age_Group>0 & vertebraecount>0) %>% group_by(startyear, Stratum, Age_Group) %>% 
              summarise(Mean_VS = mean(vertebraecount),
                        std_VS = sd(vertebraecount),
                        No = length(vertebraecount))

split_prop$Prop_WBSS<-(56.5-split_prop$Mean_VS)/(56.5-55.8)
split_prop$Prop_WBSS[split_prop$Prop_WBSS<0]<-0
split_prop$Prop_WBSS[split_prop$Prop_WBSS>1]<-1
split_prop$Prop_NSAS<-1-split_prop$Prop_WBSS


# otolith proportions
treshold <- 0.75

split_prop_otolith <- HERAS[!is.na(HERAS$Stock) & (HERAS$Prob_NSS > treshold | HERAS$Prob_NSAS > treshold | HERAS$Prob_WBSS  > treshold),]
split_prop_otolith <- split_prop_otolith[!is.na(split_prop_otolith$Age_Group),]

n_split_prop <- split_prop_otolith %>% 
                group_by(Stock, Age_Group,startyear, Stratum) %>% 
                summarise(n = n())

sum_split_prop <- n_split_prop %>% 
                  group_by(Age_Group,startyear, Stratum) %>% 
                  summarise(ind_sum = sum(n))

split_prop_otolith <- n_split_prop %>% 
                      left_join(sum_split_prop, by = c('Age_Group','startyear','Stratum')) %>% 
                      mutate(prop = n / ind_sum) %>% 
                      select(-n, -ind_sum) %>% 
                      mutate(Stock = paste0('Prop_',Stock)) %>%
                      spread(key = Stock, value = prop)

save(split_prop_otolith,split_prop,file = 'split_prop_NO.Rdata')